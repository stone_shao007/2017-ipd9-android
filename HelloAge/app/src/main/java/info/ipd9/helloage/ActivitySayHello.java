package info.ipd9.helloage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivitySayHello extends AppCompatActivity {

    private TextView tvSayHello;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_say_hello);

        tvSayHello = (TextView) findViewById(R.id.tvSayHello);
    }

    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_NAME);
        String age = intent.getStringExtra(MainActivity.EXTRA_AGE);
        String msg = String.format("Hello %s, you are %s y/o.", name, age);
        tvSayHello.setText(msg);
    }
}
