package info.ipd9.helloage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_AGE = "age";
    private  EditText etName, etAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
    }

    public void onSayHelloClick(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, ActivitySayHello.class);
        String name = etName.getText().toString();
        String age = etAge.getText().toString();
        intent.putExtra(EXTRA_NAME, name);
        intent.putExtra(EXTRA_AGE, age);

        startActivity(intent);
    }
}
