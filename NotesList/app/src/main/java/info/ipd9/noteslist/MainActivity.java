package info.ipd9.noteslist;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.AndroidException;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";
    String [] initialNotesList = {"Buy milk", "Learn Android", "Do the homeworks"};
    ArrayList<String> notesList = new ArrayList<>();
    ArrayAdapter adapter;
    private ListView lvNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate()");
        setContentView(R.layout.activity_main);

        lvNotes = (ListView) findViewById(R.id.lvNotes);
        notesList.addAll(Arrays.asList(initialNotesList));
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, notesList);
        lvNotes.setAdapter(adapter);

        lvNotes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub

                Log.d(TAG, "long clicked pos: " + pos);
                showDeleteDialog(pos);
                return true;
            }
        });

        lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int pos, long id) {
                // TODO Auto-generated method stub
                showEditDialog(pos);
                Log.d(TAG, "clicked pos: " + pos);
            }
        });
    }
    private static final String filename = "data.txt";

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart()");
        loadData();
    }

    private void loadData() {
        FileInputStream inputStream;
        try{
            inputStream = openFileInput(filename);
            Scanner input = new Scanner(inputStream);
            notesList.clear();
            while (input.hasNextLine()) {
                String note = input.nextLine();
                notesList.add(note);
            }
            adapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Error saving data", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop()");
        saveData();
    }

    private void saveData(){
        String string = "";
        for (String note:notesList) {
            string += note + "\n";
        }
        FileOutputStream outputStream;

        try{
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Error saving data", Toast.LENGTH_LONG).show();
        }
    }

    private void showEditDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit Note");

        final EditText input = new EditText(this);
        input.setText(notesList.get(pos));
        //input type
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        //setup buttons
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String note = input.getText().toString();
                Log.d(TAG, "Save note:" + note);
                notesList.set(pos,note);
                adapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showDeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Note");

        final TextView tvNote = new TextView(this);
        //input type
        tvNote.setText(notesList.get(pos));
        tvNote.setPadding(0,25,0,0);
        builder.setView(tvNote);
        //setup buttons
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Delete note:");
                notesList.remove(pos);
                adapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mi_add_note:
                showAddNoteDialog();
                Log.d(TAG,"Menu item Add Note selected");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAddNoteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add note");

        final EditText input = new EditText(this);
        //input type
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        //setup buttons
        builder.setPositiveButton("Add note", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String note = input.getText().toString();
                Log.d(TAG, "Add note:" + note);
                notesList.add(note);
                adapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
